import React, { useState } from 'react';

const ToDoForm = ({ addTask }) => {

    const [ userInput, setUserInput ] = useState('');

    const handleChange = (e) => {
        setUserInput(e.currentTarget.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        addTask(userInput);
        setUserInput("");
    }
    return (
        <form onSubmit={handleSubmit}>
            <input class="stuff" value={userInput} type="text" onChange={handleChange} placeholder="type here brother..."/>
            <button class="stuff">add</button>
        </form>
    );
};

export default ToDoForm;